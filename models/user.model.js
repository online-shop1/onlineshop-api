const mongoose = require('mongoose')
const {Schema, model} = mongoose

const userSchema = new Schema({
    fullname: {required: true, type: String},
    email: {required: true, type: String},
    password: {required: true, type: String},
    isAdmin: {required: true, type: Boolean, default: false}
})

const User = model('User', userSchema)

module.exports = User