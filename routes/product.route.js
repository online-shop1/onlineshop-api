const express = require('express')
const productController = require('../controllers/product.controller')
const {isAdmin, isAuth} = require('../utils')
const router = express.Router()

router.get('/',productController.getAll)
router.get('/:id',productController.getOne )
router.post('/',isAuth,isAdmin, productController.createProduct )
router.put('/:id',isAuth,isAdmin,productController.updateProduct )
router.delete('/:id',isAuth,isAdmin,productController.deleteProduct )

module.exports = router