const express = require('express')
const orderController = require('../controllers/order.controller')
const {isAdmin, isAuth} = require('../utils')
const router = express.Router()

router.get('/', isAuth, isAdmin, orderController.getAll)
router.get('/:id',isAuth, isAdmin, orderController.getOne)
router.post('/',isAuth, orderController.createOrder)

module.exports = router