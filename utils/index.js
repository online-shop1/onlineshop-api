const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')

const hashPassword = (plainPassword) => {
    return bcrypt.hash(plainPassword, 10)
}

const verifyPassword = (hashedPassword, plainPassword) => {
    return bcrypt.compare(plainPassword, hashedPassword);
};

const getToken = (user) => {
    return jwt.sign({
        _id: user._id,
        fullname: user.fullname,
        email: user.email,
        isAdmin: user.isAdmin
    }, process.env.JWT_SECRET, {
        expiresIn: '24h'
    })
}

const isAuth = (req,res, next)=>{
    const token = req.headers.authorization;
    if(token){
        const onlyToken = token.slice(7, token.length);
        jwt.verify(onlyToken, process.env.JWT_SECRET, (err,decode)=>{
            if(err){
                return res.status(401).send({msg: "Invalid Token"})
            }
            req.user = decode
            next()
            return
        })
     }else{
         return res.status(401).send({msg: "Token is not supplied"})
     }
}

const isAdmin = (req,res, next)=>{
    if(req.user && req.user.isAdmin){
        return next()
    }
    return res.status(401).send({msg: "Admin token is not valid"})
}
module.exports = {
    hashPassword,
    verifyPassword,
    getToken,
    isAdmin,
    isAuth
}