require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const cors = require('cors')

const productRoute  = require('./routes/product.route')
const userRoute  = require('./routes/user.route')
const orderRoute  = require('./routes/order.route')

mongoose.connect(process.env.MONGOOSE_URL,  {
    useNewUrlParser: true,
    useUnifiedTopology: true
  }).then(()=>console.log("Connected DB")).catch(error => console.log(error.reason)
)


const app = express()
app.use(bodyParser.json()) 
app.use(bodyParser.urlencoded({ extended: true }))
const port = process.env.PORT
//CORS 
app.use(cors())

app.get('/', (req, res) => res.send('Hello World!'))
app.use('/products', productRoute)
app.use('/users', userRoute)
app.use('/orders', orderRoute)


// Handling Errors
app.use((req,res,next)=>{
  const error = new Error('Not Found!');
  error.status =  404;
  next(error)
})
app.use((error, req,res,next)=>{
  res.status(error.status || 500);
  res.json({
      error:{
          message: error.message
      }
  })
})

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))