# Online-shop (BE)
Build online-shop using MERN stack

## Requirements
- Install Node
- MongoDB server(local or cloud)

## Using
Restful server with
- Express
- Mongoose + MongoDB(MongoDB Atlas)
- Authentication with jwt
- Password with bcrypt

## Features
- API Add, Edit, Remove Product
- API Add, Edit, Remove User
- API Add, Edit, Remove Order

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
Run with code
> npm start

Run with nodemon
> npm run dev

## Demo
[Link](https://online-shop123-api.herokuapp.com)

## Front End
[Link](https://gitlab.com/online-shop1/onlineshop-react)
