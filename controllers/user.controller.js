const User = require('../models/user.model')
const { hashPassword, getToken, verifyPassword } = require('../utils')

module.exports.register = async (req, res, next) => {
    const { fullname, email, password } = req.body;
    const hashPw = await hashPassword(password)
    const user = new User({
        fullname,
        email,
        password: hashPw
    })
    const newUser =await user.save()
    delete newUser._doc.password
    if(newUser){
        res.status(201).send({
            user: newUser,
            token: getToken(newUser)
        })
    }else{
        res.status(401).send({msg:"Invalid User Data"})
    }
}

module.exports.login = async(req,res,next)=>{
    const {email, password} = req.body;
    const user = await User.findOne({email : email})
    if(user){
        const verifyPw = await verifyPassword( user.password, password)
        if(verifyPw){
            delete user._doc.password
            res.status(200).send({
                user,
                token: getToken(user)
            })
        }else{
            res.status(401).send({msg:"Invalid Email or Password"})
        }
    }else{
        res.status(404).send({msg:"User not Found!"})
    }
}