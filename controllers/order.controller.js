const Order = require('../models/order.model')

module.exports.getAll = async (req, res, next) => {
    try {
        const orders = await Order.find({}).populate('user')
        if (orders.length > 0) {
            res.send(orders)
        } else {
            res.status(404).send({
                msg: "Orders not found!"
            })
        }
    } catch (error) {
        next(error)
    }
}

module.exports.createOrder = async (req, res, next) => {
    const { orderItems, shipping, payment, itemsPrice, taxPrice, shippingPrice, totalPrice, userId } = req.body;
    
    const order = new Order({
        user: userId ,
        orderItems,
        shipping,
        payment,
        itemsPrice,
        taxPrice,
        shippingPrice,
        totalPrice
    })
    const newOrder = await order.save();
    if (newOrder) {
        return res.status(201).send({ message: "New Order Created", data: newOrder })
    }
    return res.status(500).send({
        msg: "Error in creating Order."
    })
}

module.exports.getOne = async (req, res, next) => {
    try {
        const order = await Order.findOne({ _id: req.params.id }).populate({ 
            path: 'orderItems',
            populate: {
              path: 'product'
            } 
         })
        if (order) {
            res.send(order);
        } else {
            res.status(404).send("Order Not Found.")
        }
    } catch (error) {
        next(error)
    }
}