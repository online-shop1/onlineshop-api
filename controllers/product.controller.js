const Product = require('../models/product.model')


module.exports.getAll = async (req, res, next) => {
    try {
        const products = await Product.find({})
        if (products.length > 0) {
            res.send(products)
        } else {
            res.status(404).send({
                msg: "Products not found!"
            })
        }
    } catch (error) {
        next(error)
    }

}

module.exports.getOne = async (req, res, next) => {
    try {
        const { id } = req.params;
        const product = await Product.findById(id);
        if (product) {
            res.status(200).send(product)
        } else {
            res.status(404).send({
                msg: "Product not found!"
            })
        }
    } catch (error) {
        next(error)
    }
}


module.exports.createProduct = async (req, res, next) => {
    const { name, price, image, brand, category, countInStock, rating, numReviews, description } = req.body;
    const product = new Product({
        name,
        price,
        image,
        brand,
        category,
        countInStock,
        rating,
        numReviews,
        description
    })
    const newProduct = await product.save()
    if (newProduct) {
        res.status(201).send({
            msg: "Product created",
            data: newProduct
        })
    }
    return res.status(500).send({
        msg: "Error in creating Product."
    })
}

module.exports.updateProduct = async (req, res) => {
    
    const product = await Product.findById(req.params.id)
    if (product) {
        product.name = req.body.name,
        product.brand = req.body.brand,
        product.image = req.body.image,
        product.category = req.body.category,
        product.price = req.body.price,
        product.countInStock = req.body.countInStock,
        product.description = req.body.description
    }
    const updatedProduct = await product.save()
    if (updatedProduct) {
        return res.status(200).send({ msg: "Product updated.", data: updatedProduct })
        }
    return res.status(500).send({ msg: "Error in Updating product" })
}

module.exports.deleteProduct = async (req, res) => {
    const productId = req.params.id
    const deleteProduct = Product.findById(productId)
    if (deleteProduct) {
        await deleteProduct.remove()
        return res.send({ msg: "Product Deleted" })
    }
    return res.send({ msg: "Error in Deletion" })

}